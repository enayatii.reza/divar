# divar

This is a simple implementation of divar project

## Back-end

back-end is developed with Express js framwork 

libraries used in back-end: 

- @hapi/joi: for validate body and params of a request

- cors: used to solve same origin problem

- dotenv: to use env variable

- http-status: to handle response with correct status code

- jsonwebtoken: to implemente authentication with jwt method

- moment: for handling date and time

- mongoose: used to connect and communicate with mongoDB database

- multer: for handle uploading files

## Front-end

front-end is developed with React js

libraries used in front-end: 

- axios: for sending request to server

- bootstrap : to simplification css and html design

- redux: for handling state managment

- redux-persist: to maintain store's parameters