import React from 'react';
import { MapContainer } from '../Components/MapContainer';
import Header from '../Components/Header';
import { createAdvertise } from '../utils/api';

class CreateAdvertise extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            title: '',
            description: '',
            type: 'product',
            category: '',
            price: '',
            phone_number: '',
            city: 'تهران',
            region: '',

            typesOption: [
                {label: 'انتخاب نوع محصول', disabled: true},
                {label: 'محصول', name: 'product', disabled: false},
                {label: 'خدمت', name: 'service', disabled: false}
            ],

            categoriesOption: {
                product: [
                    {label: 'موبایل', name: 'mobile'},
                    {label: 'تبلت', name: 'tablet'}
                ],
                service: [
                    {label: 'نظافت کاری', name: 'cleaning'},
                    {label: 'اسباب کشی', name: 'moving'}
                ]
            },

            typeSelected: false,

            loading: false,
            error: false

        }
        this.submit = event => {
            event.preventDefault();
            console.log(this.state.file);
            this.setState({loading: true})
            createAdvertise({
                title: this.state.title,
                type: this.state.type,
                description: this.state.description,
                price: this.state.price,
                city: this.state.city,
                region: this.state.region,
                phone_number: this.state.phone_number,
                image: this.state.file
            })
                .then(response => {
                    this.setState({loading: false, error: false})
                    window.location.assign('/my-advertise')
                })
                .catch(error => {
                    this.setState({loading: false, error: true})
                })
        }
    }

    render(){
        return(
            
            <div dir='rtl'>
                <Header />
                <div className='container pb-4'>
                <div className='mt-5 text-center'>ثبت آگهی جدید</div>

                <form onSubmit={this.submit} dir='rtl' style={{backgroundColor: '#B2B2B2'}} className='rounded p-3 mt-3 w-50 mr-auto ml-auto container'>
                    <label className='float-right'>عنوان</label>
                    <input 
                        type='text'
                        onChange={evt => this.setState({title: evt.target.value})}
                        value={this.state.title} 
                        className='form-control' 
                        placeholder='عنوان آگهی را وارد کنید' />

                    <label className='float-right'>توضیحات</label>
                    <textarea 
                        type='textarea'
                        onChange={evt => this.setState({description: evt.target.value})}
                        value={this.state.description} 
                        className='form-control' 
                        placeholder='توضیحات آگهی را وارد کنید' />


                    <div className='row'>
                        <div className='col-sm'>
                            <label className='float-right'>نوع</label>
                            <select 
                                type='select'
                                onChange={evt => this.setState({type: evt.target.value, typeSelected: true})}
                                value={this.state.type} 
                                className='form-control' 
                                placeholder='نوع آگهی را انتخاب کنید'>
                                    {this.state.typesOption.map(item => {
                                            return(
                                                <option disabled={item.disabled} value={item.name}>{item.label}</option>
                                            )
                                        })
                                    }
                            </select>
                        </div>
                        {this.state.typeSelected ? 
                        <div className='col-sm'>
                        ‍‍    <label className='float-right'>دسته بندی</label>
                            <select 
                                type='select'
                                onChange={evt => this.setState({category: evt.target.value})}
                                value={this.state.category} 
                                className='form-control' 
                                placeholder='دسته بندی آگهی را انتخاب کنید'>
                                    { this.state.typeSelected && this.state.type === 'product' ? 
                                        this.state.categoriesOption.product.map(item => <option value={item.name}>{item.label}</option>) : 
                                        this.state.categoriesOption.service.map(item => <option value={item.name}>{item.label}</option>) 
                                    }
                            </select>

                        </div> : 
                        <div className='col-sm'></div>
                        }
                    </div>
                    
                    <label className='float-right'>قیمت</label>
                    <input 
                        type='text'
                        onChange={evt => this.setState({price: evt.target.value})}
                        value={this.state.price} 
                        className='form-control' 
                        placeholder='قیمت را وارد کنید' />


                    <div className='row'>
                        <div className='col-sm'>
                            <label className='float-right'>شماره موبایل</label>
                            <input 
                                type='text'
                                onChange={evt => this.setState({phone_number: evt.target.value})}
                                value={this.state.phone_number} 
                                className='form-control' 
                                placeholder='شماره موبایل را وارد کنید' />

                        </div>
                    </div>


                    <div className='row'>
                        <div className='col-sm'>
                            <label className='float-right'>شهر</label>
                            <select 
                                type='select'
                                onChange={evt => this.setState({city: evt.target.value})}
                                value={this.state.city} 
                                className='form-control' 
                                placeholder='شهر آگهی را انتخاب کنید'>
                                    <option>تهران</option>
                                    <option>قائمشهر</option>
                                    <option>شیراز</option>
                            </select>
                        </div>
                        <div className='col-sm'>
                        ‍‍    <label className='float-right'>محل</label>
                            <input 
                                type='text'
                                onChange={evt => this.setState({region: evt.target.value})}
                                value={this.state.region} 
                                className='form-control' 
                                placeholder='محل آگهی را وارد کنید' />

                        </div>
                    </div>

                    <div className='mb-3' style={{display: 'flex', flexDirection: 'column'}}>
                        ‍‍<label className='text-right'>انتخاب تصویر آگهی</label>
                        <input className='float-right' onChange={evt => this.setState({file: evt.target.files[0]})} type='file' />
                    </div>

                    {/* <MapContainer /> */}
                                    
                    <button onClick={this.submit} className='mt-3 btn-primary btn-block btn'>
                        {!this.state.loading && <div> انتشار آگهی</div>}
                        {this.state.loading && <div>صبر کنید ...</div>}
                    </button>

                    {this.state.error &&
                        <div className='text-danger text-right mt-3'>خطایی رخ داده است فرم را کنترل کنید</div>
                    }

                
                </form>
                </div>
            </div>
        );
    }

}

export default CreateAdvertise