import React from 'react';
import {connect} from 'react-redux';

import { login } from '../utils/api';
import { setSignInInfo } from '../reducers/auth';

class Login extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',

            loading: false,
            error: false
        }
        this.submit = event => {
            event.preventDefault();
            this.setState({loading: true})
            login(this.state.email, this.state.password)
                .then(response => {
                    window.location.assign('/')
                    localStorage.access_token = response.access_token;
                    this.setState({loading: false, error: false})
                    this.props.setSignInInfo(response.access_token, this.state.email)
                    console.log(response)
                })
                .catch(error => this.setState({loading: false, error: true}))
        }
    }
    render(){
        return(
            <div dir='rtl' className='container'>
                <div className='mt-5 text-center'>ورود به دیوار</div>

                <form onSubmit={this.submit} dir='rtl' style={{backgroundColor: '#B2B2B2'}} className='rounded p-3 mt-5 w-50 mr-auto ml-auto container'>
                    <label className='float-right'>ایمیل</label>
                    <input 
                        type='email'
                        onChange={evt => this.setState({email: evt.target.value})}
                        value={this.state.email} 
                        className='form-control' 
                        placeholder='ایمیل خود را وارد کنید' />

                    <label className='float-right'>رمز عبور</label>
                    <input 
                        type='password'
                        onChange={evt => this.setState({password: evt.target.value})}
                        value={this.state.password} 
                        className='form-control' 
                        placeholder='رمز عبور خود را وارد کنید' />

                    <button onClick={this.submit} className='mt-3 btn-primary btn-block btn'>
                        {!this.state.loading && <div>ورود</div>}
                        {this.state.loading && <div>صبر کنید ...</div>}
                    </button>

                    {this.state.error &&
                        <div className='text-danger text-right mt-3'>خطایی رخ داده است فرم را کنترل کنید</div>
                    }

                    <a href='/register' className='float-right text-info text-right mt-3'>ثبت نام کنید</a>

                </form>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    ...state,
    authenticated: state.auth.authenticated,
});

const mapDispatchToProps = dispatch => ({
    setSignInInfo: (access_token, email) => dispatch(setSignInInfo(access_token, email)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);

