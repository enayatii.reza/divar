import React from 'react'
import {connect} from 'react-redux';
import { register } from '../utils/api';
import { setSignInInfo } from '../reducers/auth';

class Register extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            password_confirmation: '',
            first_name: '',
            last_name: '',

            loading: false,
            error: false
        }
        this.submit = event => {
            event.preventDefault()
            this.setState({loading: true})
            register(
                this.state.email, 
                this.state.password, 
                this.state.password_confirmation,
                this.state.first_name,
                this.state.last_name
            )
                .then(response => {
                    console.log(response);
                    localStorage.access_token = response.access_token;
                    this.props.setSignInInfo(response.access_token, this.state.email)
                    window.location.assign('/')
                    this.setState({loading: false, error: false})
                })
                .catch(error => this.setState({loading: false, error: true}))
        }
    }
    render(){
        return(
            <div dir='rtl' className='container'>

                <div className='mt-5 text-center'>ثبت نام در دیوار</div>

                <form onSubmit={this.submit} dir='rtl' style={{backgroundColor: '#B2B2B2'}} className='rounded p-3 mt-5 w-50 mr-auto ml-auto container'>
                    
                    <div className='row'>
                        <div className='col-sm'>
                            <label className='float-right'>نام</label>
                            <input 
                                type='text'
                                onChange={evt => this.setState({first_name: evt.target.value})}
                                value={this.state.first_name} 
                                className='form-control' 
                                placeholder='نام خود را وارد کنید' />

                        </div>
                        <div className='col-sm'>
                            <label className='float-right'>نام خانوادگی</label>
                            <input 
                                type='text'
                                onChange={evt => this.setState({last_name: evt.target.value})}
                                value={this.state.last_name} 
                                className='form-control' 
                                placeholder='نام خانوادگی خود را وارد کنید' />

                        </div>
                    </div>
                    <label className='float-right'>ایمیل</label>
                    <input 
                        type='email'
                        onChange={evt => this.setState({email: evt.target.value})}
                        value={this.state.email} 
                        className='form-control' 
                        placeholder='ایمیل خود را وارد کنید' />

                    <label className='float-right'>رمز عبور</label>
                    <input 
                        type='password'
                        onChange={evt => this.setState({password: evt.target.value})}
                        value={this.state.password} 
                        className='form-control' 
                        placeholder='رمز عبور خود را وارد کنید' />
                    <label className='float-right'>تایید رمز عبور</label>
                    <input 
                        type='password'
                        onChange={evt => this.setState({password_confirmation: evt.target.value})}
                        value={this.state.password_confirmation} 
                        className='form-control' 
                        placeholder='رمز عبور خود را تکرار کنید' />

                    <button onClick={this.submit} className='mt-3 btn-primary btn-block btn'>
                        {!this.state.loading && <div>ثبت نام</div>}
                        {this.state.loading && <div>صبر کنید ...</div>}
                    </button>

                    {this.state.error &&
                        <div className='text-danger text-right mt-3'>خطایی رخ داده است فرم را کنترل کنید</div>
                    }

                    <a href='/login' className='float-right text-info text-right mt-3'>ورود به حساب کاربری</a>


                </form>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state,
    authenticated: state.auth.authenticated,
});

const mapDispatchToProps = dispatch => ({
    setSignInInfo: (access_token, email) => dispatch(setSignInInfo(access_token, email)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
