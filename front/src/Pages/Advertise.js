import React from 'react';
import {withRouter} from "react-router-dom";
import Header from '../Components/Header';
import { fetchAdvertiseById } from '../utils/api';

const mailUrl = 'http://localhost:3000/api/'

class Advertise extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            user: {}
        }
    }
    componentDidMount(){
        if (this.props.match.path === "/advertise/:id") {
            console.log(this.props.match.params)
            fetchAdvertiseById(this.props.match.params.id)
                .then(response => this.setState({user: response}))
                .catch(error => console.error(error))
        }   
    }
    render(){
        return(
            <div dir='rtl'>
                <Header />
                <div className='container'>
                    <div className='mr-auto ml-auto w-50'>
                        <div className='mt-3'>
                            <img style={{width: '100%'}} src={`http://localhost:3000/api/advertise/image/${this.state.user.image}`} />
                        </div>
                        <div className='text-right'>عنوان آگهی</div>
                        <div className='text-right'>{this.state.user.title}</div>

                        <div className='text-right'>ایجاد شده در</div>
                        <div className='text-right'>{this.state.user.created_at}</div>

                        <div className='text-right'>توضیحات</div>
                        <div className='text-right'>{this.state.user.description}</div>

                        <div className='text-right'>نوع آگهی</div>
                        <div className='text-right'>{this.state.user.type}</div>

                        <div className='text-right'>دسته بندی</div>
                        <div className='text-right'>{this.state.user.category}</div>

                        <div className='text-right'>قیمت</div>
                        <div className='text-right'>{this.state.user.price}</div>

                        <div className='text-right'>شهر</div>
                        <div className='text-right'>{this.state.user.city}</div>

                        <div className='text-right'>منطقه</div>
                        <div className='text-right'>{this.state.user.region}</div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Advertise);