import React from 'react';
import { MapContainer } from '../Components/MapContainer';
import InfiniteScroll from 'react-infinite-scroll-component';  
import Header from '../Components/Header';
import { fetchAdvertiseList } from '../utils/api';

const mailUrl = 'http://localhost:3000/api/'

class Home extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            items: [],
            page: 1,
          };

          this.handleRow = array => {
                let temparr = [];
                for(var i = 0 ; i < array.length ; i = i + 3)
                    if (array[i+2])
                        temparr.push({f: array[i], s: array[i+1], t: array[i+2]})      
                    else if (array[i+1]) 
                        temparr.push({f: array[i], s: array[i+1]})
                    else 
                        temparr.push({f: array[i]})
                return temparr
          }

          this.fetchMoreData = () => {
            fetchAdvertiseList(this.state.page)
                .then(response => {
                    const data = response.data
                    this.setState({page: this.state.page+1, items: this.state.items.concat(this.handleRow(response.data))});               
                })
                .catch(error => console.log(error))
          };
    }

    componentDidMount(){
        console.log(this.state.items);
        fetchAdvertiseList(this.state.page)
            .then(response => {
                const data = response.data
                this.setState({page: this.state.page+1, items: this.state.items.concat(this.handleRow(response.data))});                
            })
            .catch(error => console.log(error))
    }

    render(){
        return(
            <div dir='rtl'>
                
                <Header />

                <div dir='rtl' className='container mt-4'>

                <div className='text-right mb-2'>آخرین آگهی ها</div>
                    <div className='row'>
                        <div className='col'>
                        <InfiniteScroll
                            dataLength={this.state.items.length}
                            next={this.fetchMoreData}
                            hasMore={true}
                            loader={<h4>Loading...</h4>}>
                                {this.state.items.map((i, index) => (
                                    <div className='row' key={index}>
                                        <div onClick={() => window.location.assign(`/advertise/${i.f._id}`)} className='btn col-sm mt-4 list-adv-container ml-2' >
                                            <div className='list-inner-adv-container'>
                                                <div className='text-bold'> {i.f.title} </div>
                                                <div>
                                                    <div> {i.f.price} </div>
                                                    <div> {i.f.created_at} </div>
                                                </div>
                                            </div>  
                                            <div>
                                                <img className='list-adv-images' src={`http://localhost:3000/api/advertise/image/${i.f.image}`} />
                                            </div>  
                                        </div>
                                        {i.s ?
                                        <div onClick={() => window.location.assign(`/advertise/${i.s._id}`)} className='btn col-sm mt-4 list-adv-container ml-2' >
                                            <div className='list-inner-adv-container'>
                                                <div className='text-bold'> {i.s.title} </div>
                                                <div>
                                                    <div> {i.s.price} </div>
                                                    <div> {i.s.created_at} </div>
                                                </div>
                                            </div>  
                                            <div>
                                                <img className='list-adv-images' src={`http://localhost:3000/api/advertise/image/${i.s.image}`} />
                                            </div>  
                                        </div> : 
                                        <div className='col-sm'>
                                        </div>
                                        }
                                        {i.t ? 
                                        <div onClick={() => window.location.assign(`/advertise/${i.t._id}`)} className='btn col-sm mt-4 list-adv-container ml-2'>
                                            <div className='list-inner-adv-container'>
                                                <div className='text-bold'> {i.t.title} </div>
                                                <div>
                                                    <div> {i.t.price} </div>
                                                    <div> {i.t.created_at} </div>
                                                </div>
                                            </div>  
                                            <div>
                                                <img className='list-adv-images' src={`http://localhost:3000/api/advertise/image/${i.t.image}`} />
                                            </div>  
                                        </div> : 
                                        <div className='col-sm'>
                                        </div>
                                        }
                                    </div>
                                ))}
                        </InfiniteScroll>
                        </div> 
                    </div>
                </div>
            </div>
        );
    }

}

export default Home