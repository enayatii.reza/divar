import React from 'react';
import {withRouter} from "react-router-dom";
import Header from '../Components/Header';
import { fetchAdvertiseById, fetchCurrentUserAdv } from '../utils/api';
import InfiniteScroll from 'react-infinite-scroll-component';  

const mailUrl = 'http://localhost:3000/api/'

class MyAdvertise extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            items: [],
            page: 1,
        }
        this.fetchMoreData = () => {
            fetchCurrentUserAdv(this.state.page)
                .then(response => {
                    this.setState({page: this.state.page+1, items: this.state.items.concat(response)});               
                })
                .catch(error => console.log(error))
        };
    }
    componentDidMount(){
        console.log(this.state.items);
        fetchCurrentUserAdv(this.state.page)
            .then(response => {
                this.setState({page: this.state.page+1, items: this.state.items.concat(response)});                
            })
            .catch(error => console.log(error))

    }
    render(){
        return(
            <div dir='rtl'>
                <Header />
                <div className='p-5 container'>
                <InfiniteScroll
                            dataLength={this.state.items.length}
                            next={this.fetchMoreData}
                            hasMore={true}
                            loader={<h4>Loading...</h4>}>
                                {this.state.items.map((i, index) => (
                                    <div className='row' key={index}>
                                        <div onClick={() => window.location.assign(`/advertise/${i._id}`)} className='btn col-sm mt-4 list-adv-container ml-2' >
                                            <div className='list-inner-adv-container'>
                                                <div className='text-bold'> {i.title} </div>
                                                <div>
                                                    <div> {i.price} </div>
                                                    <div> {i.created_at} </div>
                                                </div>
                                            </div>  
                                            <div>
                                                <img className='list-adv-images' src={`http://localhost:3000/api/advertise/image/${i.image}`} />
                                            </div>  
                                        </div>
                                    </div>
                                ))}
                        </InfiniteScroll>


                </div>
            </div>
        )
    }
}

export default withRouter(MyAdvertise);