export default (
    state = {
        authenticated: false,
        access_token: null,
        user: {},
        userLoading: false
    },
    action) => {
    switch (action.type) {
        case 'SET_TOKEN':
            return {
                ...state,
                authenticated: true,
                access_token: action.access_token,
                email: action.email
            };
        case 'LOG_OUT':
            return {
                ...state,
                authenticated: false,
            }
        case 'SET_USER_INFO':
            return {
                ...state,
                user: action.user
            }
        case 'SET_USER_LOADING':
            return {
                ...state,
                userLoading: action.userLoading
            }
        default:
            return state;
    }
}

//actions

export const setSignInInfo = (access_token, email) => ({type: 'SET_TOKEN', access_token, email})
export const applyLogOut = () => ({type: 'LOG_OUT'})
export const setUserInfo = user => ({type: 'SET_USER_INFO', user})
export const setUserLoading = loading => ({type: 'SET_USER_LOADING', loading})