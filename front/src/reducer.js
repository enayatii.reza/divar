import {routerReducer} from 'react-router-redux';
import {combineReducers} from 'redux';

import auth from './reducers/auth';

export default combineReducers({
    auth,
    router: routerReducer,
});