import React, {Fragment} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {connect} from 'react-redux';

import './App.css';
import './custom.css'
import Login from './Pages/Login';
import Register from './Pages/Register';
import Home from './Pages/Home';
import CreateAdvertise from './Pages/CreateAdvertise';
import Advertise from './Pages/Advertise';
import MyAdvertise from './Pages/MyAdvertise';

class App extends React.Component {

  render(){
    return(
        <Router basename='/'>
          {this.props.isAuthenticated &&
            <Fragment>
                <Switch>
                  <Route exact path='/new-advertise' component={CreateAdvertise} />
                  <Route exact path='/my-advertise' component={MyAdvertise} />
                  <Route exact path='/' component={Home}/>
                  <Route exact path='/login' component={Home}/>
                  <Route exact path='/register' component={Home}/>
                  <Route exact path='/advertise/:id' component={Advertise} />
                </Switch>
            </Fragment>
          }
          {!this.props.isAuthenticated &&
            <Fragment>
              <Route exact path='/new-advertise' component={Login} />
              <Route exact path='/my-advertise' component={Login} />
              <Route exact path='/advertise/:id' component={Advertise} />
              <Route exact path='/' component={Home}/>
              <Route exact path='/login' component={Login}/>
              <Route exact path='/register' component={Register}/>
            </Fragment>
          }
        </Router>
      );
  }

}

const mapStateToProps = state => ({
  ...state,
  isAuthenticated: state.auth.authenticated,
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(App);