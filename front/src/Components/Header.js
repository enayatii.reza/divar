import React, { Component } from 'react';
import {connect} from 'react-redux';

import { applyLogOut } from '../reducers/auth';

export class Header extends Component {

    constructor(props){
        super(props);
        this.state = {};
        this.hrefLike = path => () => window.location.assign(path)
        this.handleSignOut = () => {
            localStorage.access_token = null;
            this.props.signOut();
            window.location.assign('/login')
        }
    }
    render() {
        return (
            <header>
                <div className='header-container pr-3 pl-3'>
                    <div className='p-2'>دیوار</div>
                    <div className='row'>
                        <div onClick={this.hrefLike('/new-advertise')} className='p-2 btn'>ثبت آگهی جدید</div>
                        <div onClick={this.hrefLike('/my-advertise')} className='p-2 btn'>آگهی های قبلی من</div>
                        <div onClick={this.handleSignOut} className='p-2 btn'>
                            {this.props.authenticated ? 'خروج' : 'ورود'}
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

const mapStateToProps = state => ({
    ...state,
    authenticated: state.auth.authenticated,
});

const mapDispatchToProps = dispatch => ({
    signOut: () => dispatch(applyLogOut()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
