import axios from 'axios';

const mailUrl = 'http://localhost:3000/api/'

export const login = (email, password) => {
    return new Promise((resolve, reject) => {
        axios({
            url: `${mailUrl}auth/login`,
            method: 'POST',
            data: {
                password: password,
                email: email
            }
        })
            .then(response => resolve(response.data))
            .catch(error => reject(error))
    })
}

export const register = (email , password, password_confirmation, first_name, last_name) => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'POST',
            url: `${mailUrl}auth/register`,
            data: {
                email: email,
                password: password,
                password_confirmation: password_confirmation,
                first_name: first_name,
                last_name: last_name
            }
        })
            .then(response => resolve(response.data))
            .catch(error => reject(error))
    })
}

export const fetchAdvertiseList = page => {
    return new Promise((resolve, reject) => {
        axios({
            url: `${mailUrl}advertise/list`,
            method: 'GET',
            params: {
                page: page
            }
        })
            .then(response => resolve(response))
            .catch(error => reject(error))
    })
}

export const fetchAdvertiseById = id => {
    return new Promise((resolve, reject) => {
        axios({
            url: `${mailUrl}advertise`,
            method: 'GET',
            params: {
                id: id
            }
        })
            .then(response => resolve(response.data))
            .catch(error => reject(error))

    })
}

export const createAdvertise = body => {
    let formData = new FormData();
    formData.append('title', body.title)
    formData.append('type', body.type)
    formData.append('description', body.description)
    formData.append('price', body.price)
    formData.append('city', body.city)
    formData.append('region', body.region)
    formData.append('phone_number', body.phone_number)
    formData.append('image', body.image)

    return new Promise((resolve, reject) => {
        axios({
            url: `${mailUrl}advertise`,
            method: 'POST',
            data: formData,
            headers: {
                Authorization: `Bearer ${localStorage.access_token}`
            }
        })
            .then(response => resolve(response.data))
            .catch(error => reject(error))
    })
}

export const fetchCurrentUserAdv = page => {
    return new Promise((resolve, reject) => {
        axios({
            url: `${mailUrl}advertise/mine`,
            method: 'GET',
            params: {
                page: page
            },
            headers: {
                Authorization: `Bearer ${localStorage.access_token}`
            }
        })
            .then(response => resolve(response.data))
            .catch(error => reject(error))
    })
}