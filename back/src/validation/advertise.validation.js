const Joi = require('@hapi/joi');

const createAdvertise = {
  body: Joi.object().keys({
    type: Joi.string().required(),
    title: Joi.string().required(),
    description: Joi.string().required(),
    price: Joi.number().required(),
    phone_number: Joi.string().required(),
    city: Joi.string().required(),
    region: Joi.string(),
    lat: Joi.number(),
    lng: Joi.number(),
    image: Joi.binary()
  }),
};

module.exports = {
    createAdvertise
};
