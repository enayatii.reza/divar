const ApiError = require('../utils/ApiError');
const httpStatus = require('http-status');
const moment = require('moment');
const advertiseService = require('../services/advertise.service');
const appRoot = require('app-root-path');

const createAdvertise = async (req, res, next) => {
    advertiseService.createAdvertise(req.body, req.decoded.sub, req.filename)
        .then(response => res.send(response))
        .catch(error => next(new ApiError(httpStatus.UNPROCESSABLE_ENTITY, error.message)))
}

const getAllAdv = async (req, res, next) => {
    advertiseService.getAdvertiseList(req.query.page)
        .then(response => res.send(response))
        .catch(error => next(new ApiError(httpStatus.UNPROCESSABLE_ENTITY, error.message)))
}

const getAdvById = async (req, res, next) => {
    advertiseService.getAdvertiseById(req.query.id)
        .then(response => res.send(response))
        .catch(error => next(error))
}

const getCurrentUserAdv = async (req, res, next) => {
    advertiseService.getCurrentUserAdvertises(req.decoded.sub, req.query.page)
        .then(response => res.send(response))
        .catch(error => next(new ApiError(httpStatus.UNPROCESSABLE_ENTITY, error.message)))
}

const downloadImage = async (req, res, next) => {
  try {
    const file = `${appRoot}/uploads/${req.params.image_name}`
    res.download(file)
  } catch {
    res.status(404).send({message: "not found"})
  }
}

module.exports = {
    createAdvertise,
    getAllAdv,
    getAdvById,
    downloadImage,
    getCurrentUserAdv
};