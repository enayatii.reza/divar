const userService = require('../services/user.service');
const authService = require('../services/auth.service');
const ApiError = require('../utils/ApiError');
const httpStatus = require('http-status');

const register = async (req, res, next) => {        
    userService.createUser(req.body)
        .then(async user => {        
            const token = await authService.generateAuthTokens(user._id);
            res.send({user, token})
        })
        .catch(error => next(new ApiError(httpStatus.UNPROCESSABLE_ENTITY, error.message)))
}

const login = async (req, res, next) => {
    const {email, password} = req.body;
    authService.loginUserWithEmail(email, password)
        .then(response => res.send(authService.generateAuthTokens(response._id)))
        .catch(error => next(new ApiError(httpStatus.UNPROCESSABLE_ENTITY , error.message)))
}

module.exports = {
    register,
    login
};