const express = require('express');
const validate = require('../middlewares/validate');
const router = express.Router();
var multer  = require('multer')
const advertisingController = require('../controllers/advertise.controller');
const advertiseValidation = require('../validation/advertise.validation');
const authentication = require('../middlewares/authenticate');
const advertiseController = require('../controllers/advertise.controller');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
        req.filename = Date.now() + '-' + file.originalname
        cb(null, req.filename)
    }
})
  
const upload = multer({storage: storage})
  
router
    .route('/')
    .post(authentication(), upload.single('image'), validate(advertiseValidation.createAdvertise), advertisingController.createAdvertise)
    .get(advertisingController.getAdvById)

router 
    .route('/list')
    .get(advertisingController.getAllAdv)

router 
    .route('/mine')
    .get(authentication(), advertiseController.getCurrentUserAdv)

router 
    .route('/image/:image_name')
    .get(advertiseController.downloadImage)

module.exports = router;