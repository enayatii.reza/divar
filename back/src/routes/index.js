const express = require('express');
const authRoute = require('./auth.route');
const advertiseRoute = require('./advertise.route');

const router = express.Router();

router.use('/auth', authRoute);
router.use('/advertise', advertiseRoute)

module.exports = router;
