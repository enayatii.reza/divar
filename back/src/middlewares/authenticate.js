const jwt  = require("jsonwebtoken");
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const { User } = require('../models');

const authenticate = () => (req, res, next) => {
    if(!req.headers.authorization)
        next(new ApiError(httpStatus.UNAUTHORIZED, 'Authentication failed'));
    else {
        const token = req.headers.authorization.split(' ')[1];    
        checkToken(token)
            .then(decoded => {
                req.decoded = decoded;
                checkUserIsValid(decoded.sub)
                    .then(() => next())
                    .catch(error => next(new ApiError(httpStatus.UNAUTHORIZED, error.message)))
            })
        .catch(error => next(new ApiError(httpStatus.UNAUTHORIZED, error.message)))
    }
}

const checkUserIsValid = userId => {
    return new Promise((resolve, reject) => {
        console.log(userId)
        User.findById(userId)
            .then(user => resolve(true))
            .catch(error => reject(error))
    })
}

const checkToken = token => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, process.env.JWT_SECRET , (error, decoded) => {
            if(decoded)
                resolve(decoded)
            else            
                reject(error)
        })
    })
}

module.exports = authenticate