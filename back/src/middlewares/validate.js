const joi = require('@hapi/joi');
const httpStatus = require('http-status');
const {pick} = require("lodash");
const ApiError = require('../utils/ApiError');

const validate = schema => (req, res, next) => {
    const validSchema = pick(schema, ['params', 'query', 'body']);
    const object = pick(req, Object.keys(validSchema));     
    const {value, error} = joi.compile(validSchema)
        .prefs({errors: {label: 'key'}})
        .validate(object)
    if (error) {
        const errorMessage = error.details.map((details) => details.message).join(', ');
        return next(new ApiError(httpStatus.UNPROCESSABLE_ENTITY, errorMessage));
    }
    else{
        console.log(value);
        next()
    }
}

module.exports = validate;