const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');

const errorConverter = (err, req, res, next) => {
  let error = err;
  if (!(error instanceof ApiError)) {
    const statusCode = error.statusCode || httpStatus.INTERNAL_SERVER_ERROR;
    const message = error.message || httpStatus[statusCode];
    error = new ApiError(statusCode, message, false, err.stack);
  }
  next(error);
};

const errorHandler = (err, req, res, next) => {
  let { statusCode, message } = err;
  res.locals.errorMessage = err.message;
    
//  just for development
  console.log(err);
    
  const response = {
    status: statusCode,
    message,
    // ...{ stack: err.stack },
  };
  res.status(statusCode).send(response);
};

module.exports = {
  errorConverter,
  errorHandler,
};
