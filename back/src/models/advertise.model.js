const mongoose = require('mongoose');

const advertiseSchema = mongoose.Schema(
    {   
        author_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users',
            required: true,
        },
        title: {
            type: String,
            required: true,
        },
        description: {
            type: String,
            required: true,
        },
        created_at: {
            type: Date,
            required: true
        },
        price: {
            type: Number,
            required: true
        },
        type: {
            type: String,
            required: true,
            enum: ["product", "service"]
        },
        phone_number: {
            type: String,
            required: true
        },
        city: {
            type: String,
            required: true
        },
        region: {
            type: String
        },
        lat: {
            type: Number
        },
        lng: {
            type: Number
        },
        image: {
            type: String
        }
    }
)

const Comment = mongoose.model('Advertise', advertiseSchema);

module.exports = Comment;