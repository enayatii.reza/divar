const { User } = require('../models');
const moment = require("moment");
const jwt = require('jsonwebtoken');
const httpStatus = require('http-status');

const ApiError = require('../utils/ApiError');

const loginUserWithEmail = (email, password) => {
    return new Promise((resolve, reject) => {
        User.findOne({email: email})
            .then(user => {
                if(user.password === password)
                    resolve(user)
                else
                    reject(new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password'))
            })
            .catch(error => reject(error))
    })
}

const generateToken = (userId, secret = process.env.JWT_SECRET) => {
    const payload = {
        sub: userId,
        iat: moment().unix(),
        exp: moment().add(process.env.JWT_ACCESS_EXPIRATION_MINUTES, 'minutes').unix(),
    };
    return jwt.sign(payload, secret)
}

const generateAuthTokens = user => {
       const access_token = generateToken(user)
       return {
           access_token: access_token
       }
}


module.exports = {
    loginUserWithEmail,
    generateAuthTokens,
    generateToken
};
  