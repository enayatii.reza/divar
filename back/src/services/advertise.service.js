const { Advertise } = require('../models');
const moment = require('moment');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const { reject } = require('lodash');
const imageThumbnail = require('image-thumbnail');

const createAdvertise = async (body, userId, imageName) => {
    return new Promise((resolve, reject) => {
        Advertise.create({
            author_id: userId,
            type: body.type,
            title: body.title,
            description: body.description,
            phone_number: body.phone_number,
            price: body.price,
            city: body.city,
            region: body.region,
            lat: body.lat,
            lng: body.lng,
            created_at: moment.now(),
            image: imageName
        })
            .then(response => resolve(response))
            .catch(error => reject(error))
    })
}

const getAdvertiseList = async (page_number = 1, per_page = 18) => {
    console.log(per_page)
    return new Promise((resolve, reject) => {
        Advertise.find({}, ['_id', 'title', 'city', 'price', 'created_at', 'image'], {sort: {created_at: -1}, skip: (page_number-1)*per_page, limit: per_page*1})
            .then(response => resolve(response))
            .catch(error => reject(error))
    })
}

const getAdvertiseById = async userId => {
    return new Promise((resolve, reject) => {
        Advertise.findById(userId)
            .then(response => {
                if(response === null)
                    reject(new ApiError(httpStatus.NOT_FOUND, 'Advertise not found'))
                resolve(response)
            })
            .catch(error => reject(new ApiError(httpStatus.UNPROCESSABLE_ENTITY, error.message)))
    })
}

const getCurrentUserAdvertises = async (userId, page_number = 1, per_page = 11) => {
    return new Promise((resolve, reject) => {
        Advertise.find({author_id: userId},  ['_id', 'title', 'city', 'price', 'created_at', 'image'], {sort: {created_at: -1}, skip: (page_number-1)*per_page, limit: per_page*1})
            .then(response => resolve(response))
            .catch(error => reject(error))    
    })
}

module.exports = {
    createAdvertise,
    getAdvertiseList,
    getAdvertiseById,
    getCurrentUserAdvertises
};