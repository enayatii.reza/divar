const { User } = require('../models');

const createUser = userBody => {    
    return new Promise((resolve, reject) => {
        if(userBody.password === userBody.password_confirmation)
        User.create(userBody)
        .then(response => {            
            resolve(response)})
        .catch(error => {            
            reject(error)
        })
    })
}


module.exports = {
    createUser
}