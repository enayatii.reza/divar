const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');
const { errorConverter, errorHandler } = require('./middlewares/error');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const multer = require('multer');

dotenv.config({ path: path.join(__dirname, '../.env') });

const indexRouter = require('./routes/index');

const app = express();

// handle cors problem
app.use(cors())

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// TODO don't know
app.use(logger('dev'));

// parse json request body
app.use(express.json());

// parse urlencoded request body
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// Handle Routes
app.use('/api', indexRouter);

// Handle Errors :

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError[404]());
});

// convert error to ApiError, if needed
app.use(errorConverter);

// handle error
app.use(errorHandler);

mongoose.connect( 
    process.env.MONGODB_URL,
    { 
      useCreateIndex: true, 
      useNewUrlParser: true,
      useUnifiedTopology: true
    });


module.exports = app;
